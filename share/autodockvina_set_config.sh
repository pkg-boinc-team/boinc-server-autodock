#!/bin/bash

# Set configuration parameters 
#
# This script is sourced from others
#
set -e

if [ $(basename $0) = "autodock-vina_set_config.sh" ]; then
	echo "E: autodock-vina_set_config.sh is not meant to be executed directly."
	exit 1
fi

# For copying files between directories, this is a safe bet when invoked with ./
BOINC_SOURCEDIR_ABS=$(readlink -f "$BOINC_SOURCEDIR")

## Check if the configuration variables are already set and initialize them if not
if [ -z "$BOINC_USER" ];            then BOINC_USER=`logname`; fi
if [ -z "$BOINC_USER" ];            then BOINC_USER=$LOGNAME; fi
if [ "$BOINC_USER" = "root" ];      then echo "E: Please do not install with root as BOINC_USER!"; exit 1; fi
if [ -z "$BOINC_HOMEDIR" ];         then BOINC_HOMEDIR=$HOME; fi
if [ -z "$BOINC_WWWGROUP" ];        then BOINC_WWWGROUP="www-data"; fi
if [ -z "$BOINC_PROJECTNAME" ];     then BOINC_PROJECTNAME="autodockvina"; fi
if [ -z "$BOINC_PROJECTNICENAME" ]; then BOINC_PROJECTNICENAME="AutoDockVina@Home"; fi
if [ -z "$BOINC_DBUSER" ];          then BOINC_DBUSER=$BOINC_USER; fi
if [ -z "$BOINC_DBPASS" ];          then BOINC_DBPASS="boincpass125"; fi
if [ -z "$BOINC_INSTALLROOT" ];     then BOINC_INSTALLROOT="/tmp/boinc-server-autodock-vina"; fi
if [ -z "$BOINC_HOSTNAME" ];        then BOINC_HOSTNAME=`hostname`; fi
if [ -z "$BOINC_PROJECTURL" ];      then BOINC_PROJECTURL="http://"`hostname -f`; fi

if [ -z "$BOINC_SOURCEDIR" ];       then BOINC_SOURCEDIR=$(dirname $0); fi
if [ -z "$BOINC_FETCHAPPSSCRIPT" ]; then
	for dirname in "$BOINC_SOURCEDIR"/bin "$BOINC_SOURCEDIR" "/usr/share/boinc-server-maker" 
	do
		if [ -x "$dirname/fetch_example_applications.sh" ]; then
			BOINC_FETCHAPPSSCRIPT="$dirname/fetch_example_applications.sh"
			break
		fi
	done
fi

## Option for a local computer without domain name: use IP
# hostip=`hostname -I | head -n 1 | tr -d ' '` # Get IP and trim whitespaces
# if [ -z "$BOINC_PROJECTURL" ];      then BOINC_PROJECTURL=`echo "http://${hostip}"`; fi

if [ -z "$RACCOON_DIR" ];           then RACCOON_DIR="${BOINC_HOMEDIR}/raccoon"; fi
if [ -z "$RACCOON_LIBRARIES_DIR" ]; then RACCOON_LIBRARIES_DIR="${RACCOON_DIR}/libraries"; fi 
if [ -z "$RACCOON_DATA_DIR" ];      then RACCOON_DATA_DIR="${RACCOON_DIR}/data"; fi
