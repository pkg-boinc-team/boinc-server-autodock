
## TODO: description

from numpy import array, loadtxt, extract, amin, mean
from scipy.cluster.vq import vq, kmeans, kmeans2, whiten
import sys

infile   = sys.argv[1]
outfile  = sys.argv[2]
points   = loadtxt(infile, usecols=(1, 2, 3))            # Load a group of observations - 3-dimensional points
energies = loadtxt(infile, usecols=[4])                  # Load energy values for the observations
names    = loadtxt(infile, dtype='string', usecols=[0])  # Load ligand names for the observations

K        = 3                                             # Desired number of clusters
result   = kmeans2(points, K)                            # Classify observations into K clusters 
N        = len(result[1])                                # Number of processed observations

pmlfile  = open(outfile, 'a', 100)

## Get information about each of the clusters
for c in range(K):
    condition = result[1] == c    
    cluster = extract(condition, energies)
    if len(cluster) > 0:
        CLUSTER_MIN=amin(cluster)
        print "  Cluster %d: center [%3.3f, %3.3f, %3.3f], %d members, minimal energy %3.3f, mean energy %3.3f" % (c, result[0][c][0], result[0][c][1], result[0][c][2], len(cluster), CLUSTER_MIN, mean(cluster))
        pmlfile.write("pseudoatom c%d, color=yellow, pos=[%3.3f, %3.3f, %3.3f];" % (c, result[0][c][0], result[0][c][1], result[0][c][2])) 
        pmlfile.write("pseudoatom c%d_label, label=\"%3.3f\", pos=[%3.3f, %3.3f, %3.3f];" % (c, CLUSTER_MIN, result[0][c][0], result[0][c][1], result[0][c][2]))
        pmlfile.write("show spheres, c%d;\n" % c)

pmlfile.write("set label_position,(3,2,1)\n")
pmlfile.close()
