#!/bin/bash

if [ 2 -eq "$#" -a -f "$1" -a -f "$2" ]; then

  REF_LIG=$1
  TEST_LIG=$2  
  TANIMOTO_COEFF=`obabel $REF_LIG $TEST_LIG ---errorlevel 1 -ofpt 2>/dev/null | awk -F' ' 'match($0,"Tanimoto from '$REF_LIG' = ([0-9.]+)",a){ print a[1]; exit 1 }'`
  echo "$TANIMOTO_COEFF"

else
  echo "Please provide the filenames of the reference ligand and the test ligand."
fi
