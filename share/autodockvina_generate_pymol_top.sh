#!/bin/bash

# This script is called from autodockvina_compare_results.sh

set -e

PART=256   # Number of files to process at once (we need to split files 
           # in order to manage large amounts of them). In case of errors
           # try to decrease this number.

if [ 1 -eq "$#" -a -d "$1" ]; then
  find "$1" -maxdepth 1 -name "*_1.pdbqt" -print0 | xargs -0 -n $PART -r $0
else
  for PDBQT in $* 
  do
      MODEL1_DIR=$(dirname $PDBQT)
      DATFILE=${DIRNAME}.dat
      LIGNAME=$(basename $PDBQT _1.pdbqt)

      if [ -f $PDBQT ]; then
          E=$(grep -m 1 "VINA RESULT:" $PDBQT | sed 's/  */ /g' | cut -d' ' -f4)
          if [ 1 -eq `echo "$E < $THRESHOLD" | bc` ]; then
              TC=$(./get_tanimoto_coeff.sh $REF_LIGAND $PDBQT)
              LOADLIST=$LOADLIST" load $PDBQT, $LIGNAME;"
              COMLIST=$COMLIST" COM $LIGNAME, fmt=1, value=$E, tanimoto=$TC;"
          #else
          #    echo "Skipping ligand $PDBQT -- predicted binding energy is below the threshold" 
          #    #TODO: remove the files to save disk space?
          fi
      else
          echo "Input file $PDBQT is missing"
      fi
done

  if [ ! -z "$LOADLIST" -a ! -z "$COMLIST" ]; then
      ## Load molecule models and calculate centers of mass using PyMol:
      pymol -c -q -d "import com; feedback disable,all,actions; $LOADLIST $COMLIST" | tail -n +2 >> $DATFILE
  #else
      #echo "No ligands selected in the next $PART ligands"
  fi
fi
