#!/bin/bash

set -e

PART=128   # Number of files to process at once (we need to split files 
           # in order to manage large amounts of them). In case of errors
           # try to decrease this number.

if [ 1 -eq "$#" -a -d "$1" ]; then
  find "$1" -maxdepth 1 -name "*_1" -print0 | xargs -0 -n $PART -r $0
else
  DATFILE=${DIRNAME}.dat
  for PDBQT in $*
  do
      MODEL1_DIR=$(dirname $PDBQT)
      LIGNAME=$(basename $PDBQT _1)

      if [ -f $PDBQT ]; then
        E=$(grep -m 1 "VINA RESULT:" $PDBQT | sed 's/  */ /g' | cut -d' ' -f4)
        LOADLIST=$LOADLIST" load $PDBQT, $LIGNAME;"
        COMLIST=$COMLIST" COM $LIGNAME, fmt=0, value=$E;"
      else
        echo "Input file $PDBQT is missing"
      fi
  done

  if [ -z "$LOADLIST" -o -z "$COMLIST" ]; then
      echo "Please provide the name of the directory with PDBQT files."
      exit 1
  else
      ## Load molecule models and calculate centers of mass using PyMol
      pymol -c -q -d "import com; feedback disable,all,actions; $LOADLIST $COMLIST" | tail -n +2 >> $DATFILE
  fi

fi
