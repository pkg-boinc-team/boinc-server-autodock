# Comparison of a set of docked ligands with the reference one;
# point color represents the absolute value of predicted binding energy.
# 
# Created by Natalia Nikitina (nevecie@yandex.ru)
# 
set xlabel "Distance from the reference ligand"
set ylabel "Tanimoto coefficient with the reference ligand"
set palette defined ( 0 "green", 1 "yellow", 2 "red" )
set term png
set output graphfile
plot datafile using 1:2:3 with points palette title ""
