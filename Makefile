VERSION=$(shell cat VERSION)

PREFIX=/usr/
DESTDIR=/

all:

install:
	mkdir -p $(DESTDIR)/$(PREFIX)/share/boinc-server-autodock
	cp -r share/* bin/* $(DESTDIR)/$(PREFIX)/share/boinc-server-autodock
	sed -i "s/^version=.*/version=$(cat VERSION)/" $(DESTDIR)/$(PREFIX)/share/boinc-server-autodock/*.sh

clean:
	find . -name "*.pyc" -delete

dist:
	p=`pwd`; \
	d=`basename $$p`; \
	echo "Taring up directory '$$d'." ; \
	cd .. ; \
	tar --exclude .git --exclude debian --exclude "*.pyc" -cJvf boinc-server-autodock_$(VERSION).tar.xz $$d

.PHONY: dist clean
