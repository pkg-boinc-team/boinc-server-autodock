#!/bin/bash

set -e

version=0.0.0
if [ "-version" = "$1" -o "--version" = "$1" ]; then
   echo "$version"
   exit 1
fi

if [ "-h" = "$1" -o "-help" = "$1" -o "--help" = "$1" ]; then
        cat <<EOHELP
$(basename $0|tr "a-z" "A-Z")                   BOINC-SERVER-AUTODOCK                   $(basename $0|tr "a-z" "A-Z")

NAME

  $(basename $0) - convert a set of PDBQT files to a set of PDB files, 
                   each containing a description of the first model.

SYNOPSIS

  $(basename $0) [--help]
  $(basename $0) <Directory with PDBQT files>

DESCRIPTION

  This script converts every PDBQT file found in the directory specified
  by the first command line argument to a PDB file and cuts the latter to
  only the first docked ligand model. Result files are stored in a directory
  named <Directory with PDBQT files>_pdb.

SEE ALSO

  http://mgltools.scripps.edu
  http://autodock.scripps.edu

COPYRIGHT

  This script is released under the same license as BOINC.

AUTHORS

  The script was crafted by Natalia Nikitina (nevecie@yandex.ru).

EOHELP
        exit
fi


if [ 1 -eq "$#" -a -d "$1" ]; then
  
  PDB_DIR=`basename $1`_pdb ; mkdir -p $PDB_DIR

  for DOCKED_PDBQT in `find $1 -name "*_1"`
  do 
    DOCKED_PDB=`basename $DOCKED_PDBQT` 
    cut -c-66 $DOCKED_PDBQT | sed -n "/MODEL 1/,/MODEL 2/p" | head -n -1 > ${PDB_DIR}/${DOCKED_PDB}.pdb
  done

else

  echo "Please provide the name of the directory with output files in PDBQT format."

fi
