#!/bin/bash

set -e

version=0.0.0
if [ "-version" = "$1" -o "--version" = "$1" ]; then
   echo "$version"
   exit 1
fi

if [ -z "$BOINC_SOURCEDIR" ]; then BOINC_SOURCEDIR=$(dirname $(realpath $0)); fi

## Set configuration parameters
if [ -r "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then
   . "$BOINC_SOURCEDIR"/autodockvina_set_config.sh
fi

if [ "-h" = "$1" -o "-help" = "$1" -o "--help" = "$1" ]; then
        cat <<EOHELP
$(basename $0|tr "a-z" "A-Z")                   BOINC-SERVER-AUTODOCK                   $(basename $0|tr "a-z" "A-Z")

NAME

  $(basename $0) - Get number of new batch for BOINC workunits 

SYNOPSIS

  $(basename $0) [--help]

DESCRIPTION

  This script retrieves the next batch number to create.
  All parameters are specified by environment variables.

     BOINC_PROJECTNAME   defines the unique short name of the project, which also serves
                         as the database name.
     BOINC_DBUSER        defines the user name for access to MySQL database.
     BOINC_DBPASS        defines the password for access to MySQL database.

SEE ALSO

  http://wiki.debian.org/BOINC

COPYRIGHT

  This script is released under the same license as BOINC.

AUTHORS

  Natalia Nikitina

EOHELP
        exit
fi

if [ ! -r "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then
   echo "E: Could not read '$BOINC_SOURCEDIR/autodockvina_set_config.sh'"
   exit 1
fi

BATCH=`mysql -u $BOINC_DBUSER -p$BOINC_DBPASS -N -s -e "use $BOINC_PROJECTNAME; select MAX(batch) from workunit;" 2> /dev/null`
if [ -z "$BATCH" ]; then
  echo "0"
else
  let BATCH+=1
  echo "$BATCH"
fi
