#!/bin/bash

set -e

version=0.0.0
if [ "-version" = "$1" -o "--version" = "$1" ]; then
   echo "$version"
   exit 1
fi

## Set configuration parameters
if [ -z "$BOINC_SOURCEDIR" ]; then BOINC_SOURCEDIR=$(dirname $0); fi

if [ "-h" = "$1" -o "-help" = "$1" -o "--help" = "$1" ]; then
        cat <<EOHELP
$(basename $0|tr "a-z" "A-Z")                   BOINC-SERVER-AUTODOCK                   $(basename $0|tr "a-z" "A-Z")

NAME

  $(basename $0) - process a set of docking results comparing 
                   them with a given "reference" docked ligand,
                   writing coordinates of ligand centers, energy
                   values and Tanimoto coefficients of similarity
                   with the reference ligand for further analysis
                   and visualisation, plotting a comparison diagram. 

SYNOPSIS

  $(basename $0) [--help]
  $(basename $0) <Directory with PDBQT files of the docking results> <Name of the set of docking results> <PDBQT model of the reference ligand>

DESCRIPTION

  This script performs the following steps:

  1) Copies the first docked model from each PDBQT file found in <Directory with PDBQT files>
     to a file in <Directory with PDBQT files>_pdbqt. Input files should be named by template 
     specified by BOINC project settings.

  2) Writes to <Directory with PDBQT files>.dat the names of the ligands, coordinates of their 
     centers and predicted docking energy values.

  3) For each docked ligand, writes to <Directory with PDBQT files>_final.dat the distance between
     this one and the reference ligand, Tanimoto coefficient of their similarity, predicted docking
     energy value and the ligand name.

  4) Plots to <Directory with PDBQT files>_final.png a diagram with the distance between 
     the reference ligand and the tested one along X axis, Tanimoto coefficient between 
     the reference ligand and the tested one along Y axis; point color represents the absolute 
     value of predicted binding energy.

SEE ALSO

  http://mgltools.scripps.edu
  http://autodock.scripps.edu

COPYRIGHT

  This script is released under the same license as BOINC.

AUTHORS

  The script was crafted by Natalia Nikitina (nevecie@yandex.ru).

EOHELP
        exit
fi


## Export the first one conformation for each of the docked ligands
if [ 3 -eq "$#" -a -d "$1" -a -f "$3" ]; then

  DIRNAME=$(basename $1)
  SETNAME="$2"
  REF_LIGAND="$3"

  MODEL1_DIR=${DIRNAME}_pdbqt
  if [ -d $MODEL1_DIR ]; then
      mv $MODEL1_DIR ${MODEL1_DIR}_`date '+%s'`  
  fi
  mkdir $MODEL1_DIR

  for DOCKED_PDBQT in `find $1 -mindepth 1 -name "*_1"`
  do
    OUTFILE=`basename $DOCKED_PDBQT _1`
    # pdbqt file extension is needed for obabel to know file format
    cat $DOCKED_PDBQT | sed -n "/MODEL 1/,/MODEL 2/p" | head -n -1 > ${MODEL1_DIR}/${OUTFILE}_1.pdbqt
  done

  echo "  Results were exported to $MODEL1_DIR"

else

  echo "Please provide the name of the directory with output files in PDBQT format, the name of ligands set 
        and a file with a reference docked ligand in PDBQT format."
  exit 1

fi

## Get ligand name, center coordinates and the best docking energy value for each of the docked ligands
DATFILE=${DIRNAME}.dat
FINALFILE=${DIRNAME}_final.dat
GRAPHFILE=${DIRNAME}_final.png
LOADLIST=''
COMLIST=''

if [ -f $DATFILE ]; then
    mv $DATFILE ${DATFILE}_`date '+%s'`
fi

## Set variables for the reference ligand
REFNAME=$(basename $REF_LIGAND) 
REF_E=$(grep -m 1 "VINA RESULT:" $REF_LIGAND | sed 's/  */ /g' | cut -d' ' -f4)
REF_TC=1.0
## Set threshold = predicted energy value of the reference ligand + 1
THRESHOLD=$(echo $REF_E + 1 | bc)

## Write information about the reference ligand in the first line of the data file
LOADLIST="load $REF_LIGAND, $REFNAME;"
COMLIST="COM $REFNAME, fmt=1, value=$REF_E, tanimoto=$REF_TC;"
pymol -c -q -d "import com; feedback disable,all,actions; $LOADLIST $COMLIST" | tail -n +2 >> $DATFILE

## Export variable values to execute the next script that writes data table in final format
export REF_LIGAND=$REF_LIGAND
export THRESHOLD=$THRESHOLD # Threshold to select the best ligands
export DIRNAME=$DIRNAME
bash ${BOINC_SOURCEDIR}/autodockvina_generate_pymol_top.sh $MODEL1_DIR

echo "# Distance | Tanimoto coefficient | Energy value | Ligand name " > $FINALFILE
awk -F' ' 'NR==1 { XREF=$2; YREF=$3; ZREF=$4 } { D=((XREF-$2)**2+(YREF-$3)**2+(ZREF-$4)**2)**0.5; print D, $6, $5, $1 }' $DATFILE >> $FINALFILE

echo "  Data summary was written to $FINALFILE"
gnuplot -e "datafile='$FINALFILE'; graphfile='$GRAPHFILE'; set title '$SETNAME';" graph.gnuplot
echo "  Diagram was saved to $GRAPHFILE"
