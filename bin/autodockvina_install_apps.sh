#!/bin/bash

set -e

version=0.0.0
if [ "-version" = "$1" -o "--version" = "$1" ]; then
   echo "$version"
   exit 1
fi

if ! which realpath; then
	echo "E (autodock_vina_install_apps.sh): This application needs the tool realpath in its path."
	echo "                                   It is provided by a cognate Debian package or with the";
	echo "                                   coreutils >= 8.23."
	exit 1
fi

if [ -z "$BOINC_SOURCEDIR" ]; then BOINC_SOURCEDIR=$(dirname $(realpath $0)); fi

if [ -r  "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then 
   . "$BOINC_SOURCEDIR"/autodockvina_set_config.sh
fi 

if [ "-h" = "$1" -o "-help" = "$1" -o "--help" = "$1" ]; then
  cat <<EOHELP
$(basename $0|tr "a-z" "A-Z")                   BOINC-SERVER-AUTODOCK                   $(basename $0|tr "a-z" "A-Z")

NAME

  $(basename $0) - installation of AutoDock Vina application

SYNOPSIS

  $(basename $0) [--help]

DESCRIPTION

  This script installs AutoDock Vina application for the existing BOINC project
  with the specified name. The final BOINC application contains of the AutoDock
  Vina scientific application and the wrapper. 

  All parameters are specified by environment variables.

    BOINC_PROJECTNAME       defines the unique short name of the project, which 
                            also serves as the database name ($BOINC_PROJECTNAME).
    BOINC_SOURCEDIR         defines the path to directory with intermediate scripts 
                            to install and configure the BOINC project ($BOINC_SOURCEDIR).
    BOINC_INSTALLROOT       defines the path to the directory where the project is 
                            installed ($BOINC_INSTALLROOT).
    BOINC_FETCHAPPSSCRIPT   defines the location of the script that fetches BOINC
                            example applications ($BOINC_FETCHAPPSSCRIPT).

SEE ALSO

  http://wiki.debian.org/BOINC
  http://mgltools.scripps.edu
  http://autodock.scripps.edu

COPYRIGHT

  This script is released under the same license as BOINC.

AUTHORS

  Natalia Nikitina

EOHELP
        exit
fi

if [ ! -r "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then
   echo "E (autodockvina_install_apps.sh): Could not read '$BOINC_SOURCEDIR/autodockvina_set_config.sh'"
   exit 1
fi

## Install autodock-vina application 
echo "I (autodockvina_install_apps.sh): Installing AutoDock Vina application..."

longappver=$(apt-cache show autodock-vina | grep ^Version | tail -1 | cut -f2 -d\  )
appver=$(echo $longappver|cut -d . -f-2)

longappverwrapper=$(apt-cache show boinc-app-examples | grep ^Version | tail -1 | cut -f2 -d\  )
appverwrapper=$(echo $longappverwrapper|cut -d . -f-2)

#Create temporary directories
TEMPDIR=/tmp/boinc-server-autodock
TEMPDIR_VINA=${TEMPDIR}/fetch-vina
TEMPDIR_APPS=${TEMPDIR}/fetch-app
mkdir -p ${TEMPDIR_APPS} ${TEMPDIR_VINA}

#Get current Vina versions in Debian:
echo "I (autodockvina_install_apps.sh): (autodockvina_install_apps.sh): Call own script to collect vina for many platforms"
cd ${TEMPDIR_VINA}
packagename="autodock-vina" ${BOINC_SOURCEDIR}/fetch_autodock_vina.sh .
echo "I (autodockvina_install_apps.sh): (autodockvina_install_apps.sh): Call boinc-server-maker script to collect wrapper for many platforms"
cd ${TEMPDIR_APPS}
packagename="boinc-app-examples" /usr/share/boinc-server-maker/fetch_example_applications.sh .

#Create temporary directory and get current wrapper versions:

for boincplat in `ls ${TEMPDIR_APPS}/apps/wrapper/${appverwrapper}/`
do
  appfileswrapper=`ls -1 ${TEMPDIR_APPS}/apps/wrapper/${appverwrapper}/${boincplat}/ | wc -l`
  if [ $appfileswrapper -eq 0 ]; then
    rm -rf ${TEMPDIR_APPS}/apps/wrapper/${appverwrapper}/${boincplat}
    rm -rf ${TEMPDIR_VINA}/apps/vina/${appver}/${boincplat}
  fi    
done

for boincplat in `ls ${TEMPDIR_VINA}/apps/vina/${appver}/`
do
  appfiles=`ls -1 ${TEMPDIR_VINA}/apps/vina/${appver}/${boincplat}/ | wc -l`
  if [ $appfiles -eq 0 ]; then
    rm -rf ${TEMPDIR_APPS}/apps/wrapper/${appverwrapper}/${boincplat}
    rm -rf ${TEMPDIR_VINA}/apps/vina/${appver}/${boincplat}
  fi
done

appdir=${BOINC_INSTALLROOT}/${BOINC_PROJECTNAME}/apps/autodock-vina
echo "I (autodockvina_install_apps.sh): Executing sudo mkdir '$appdir'"
sudo mkdir -p $appdir
# during installation the permissions are somewhat too permissive
sudo chmod 777 $appdir

#for boincplat in `ls ${appdir}/${appver}/`
for boincplat in `ls ${TEMPDIR_VINA}/apps/vina/*`
do
  # Check if autodock-vina or vina
  for f in ${appdir}/${appver}/${boincplat}/vina_*
  do
    fname=`basename $f`
    source="${appdir}"/"${appver}"/"${boincplat}"/"${fname}"
    target="${appdir}"/"${appver}"/"${boincplat}"/"autodock-${fname}"
    if ! mv $source $target ; then
       echo "E (autodockvina_install_apps.sh): Failed moving '$source' to '$target'."
       exit 1
    fi
  done
done
cp -r ${TEMPDIR_VINA}/apps/vina/* ${appdir}/

## Copy wrapper binaries to appropriate directories
for boincplat in `ls apps/wrapper/${appverwrapper}/`
do
  cp ${TEMPDIR_APPS}/apps/wrapper/${appverwrapper}/${boincplat}/wrapper_${appverwrapper}_${boincplat} ${appdir}/${appver}/${boincplat}/wrapper_${appver}_${boincplat}
  echo "<job_desc>
    <task>
        <application>autodock-vina</application>
        <append_cmdline_args/>
        <stdout_filename>stdout</stdout_filename>
        <fraction_done_filename>fraction.done</fraction_done_filename>
    </task>
</job_desc>" > ${appdir}/${appver}/${boincplat}/autodock-vina_job_${appver}_${boincplat}.xml
done

echo "I (autodockvina_install_apps.sh): Installing Windows and Mac binaries side by side"

## Separate way to install binaries for Windows and Mac since outside Debian
TEMPDIR_WIN_MAC=/tmp/boinc-server-autodock/windows_mac
mkdir -p ${TEMPDIR_WIN_MAC}
cd ${TEMPDIR_WIN_MAC}
/usr/share/boinc-server-maker/get_latest_windows_wrappers.sh
mkdir -p ${TEMPDIR_WIN_MAC}/fetch-app-win/windows_intelx86
mkdir -p ${TEMPDIR_WIN_MAC}/fetch-app-win/windows_x86_64
echo "I (autodockvina_install_apps.sh): getting directory structure for apps"
mv ${TEMPDIR_WIN_MAC}/wrapper_windows_intelx86.exe \
   ${TEMPDIR_WIN_MAC}/fetch-app-win/windows_intelx86/wrapper_${appver}_windows_intelx86.exe
mv ${TEMPDIR_WIN_MAC}/wrapper_windows_x86_64.exe \
   ${TEMPDIR_WIN_MAC}/fetch-app-win/windows_x86_64/wrapper_${appver}_windows_x86_64.exe
cp ${BOINC_SOURCEDIR}/autodockvina_binaries/autodock-vina_windows_intelx86.exe \
   ${TEMPDIR_WIN_MAC}/fetch-app-win/windows_intelx86/autodock-vina_${appver}_windows_intelx86.exe
cp ${BOINC_SOURCEDIR}/autodockvina_binaries/autodock-vina_windows_x86_64.exe \
   ${TEMPDIR_WIN_MAC}/fetch-app-win/windows_x86_64/autodock-vina_${appver}_windows_x86_64.exe

echo "I (autodockvina_install_apps.sh): Copying to windows apps to '${appdir}/${appver}/'"
cp -r ${TEMPDIR_WIN_MAC}/fetch-app-win/* ${appdir}/${appver}/

for boincplat in windows_intelx86 windows_x86_64 i686-apple-darwin x64_64-apple-darwin
do
  mkdir -p "${appdir}"/"${appver}"/"$boincplat"
  echo "<job_desc>
    <task>
        <application>autodock-vina</application>        
        <append_cmdline_args/>
        <stdout_filename>stdout</stdout_filename>
        <fraction_done_filename>fraction.done</fraction_done_filename>
    </task>
</job_desc>" > "${appdir}"/"${appver}"/"$boincplat"/"autodock-vina_job_${appver}_${boincplat}.xml"
done

rm -f ${TEMPDIR_WIN_MAC}/*windows*.pdb

echo "I (autodockvina_install_apps.sh): Preparing version.xml files"


for boincplat in `ls ${appdir}/${appver}/`
do
if [[ $boincplat =~ windows* ]]; then
   # Windows - because of the .exe suffix
   echo "<version>
   <file>
      <physical_name>wrapper_${appver}_${boincplat}.exe</physical_name>
      <main_program/>
   </file>
   <file>
      <physical_name>autodock-vina_${appver}_${boincplat}.exe</physical_name>
      <logical_name>autodock-vina</logical_name>
   </file>
   <file>
      <physical_name>autodock-vina_job_${appver}_${boincplat}.xml</physical_name>
      <logical_name>job.xml</logical_name>
   </file>
   </version>" > ${BOINC_INSTALLROOT}/${BOINC_PROJECTNAME}/apps/autodock-vina/$appver/$boincplat/version.xml
else
   # Linux and Mac
   echo "<version>
   <file>
      <physical_name>wrapper_${appver}_${boincplat}</physical_name>
      <main_program/>
   </file>
   <file>
      <physical_name>autodock-vina_${appver}_${boincplat}</physical_name>
      <logical_name>autodock-vina</logical_name>
   </file>
   <file>
      <physical_name>autodock-vina_job_${appver}_${boincplat}.xml</physical_name>
      <logical_name>job.xml</logical_name>
   </file>
   </version>" > ${BOINC_INSTALLROOT}/${BOINC_PROJECTNAME}/apps/autodock-vina/$appver/$boincplat/version.xml
fi
done

echo "I (autodockvina_install_apps.sh): Editing project.xml file"

## Edit project.xml file to register autodock-vina application to the BOINC project
echo "<boinc>
  <app>
     <name>autodock-vina</name>
     <user_friendly_name>autodock-vina</user_friendly_name>
  </app>" > ${BOINC_INSTALLROOT}/${BOINC_PROJECTNAME}/project.xml

for boincplat in `ls ${appdir}/${appver}/`
do
  echo "<platform>
  <name>$boincplat</name>
  <user_friendly_name>$boincplat</user_friendly_name>
  </platform>" >> ${BOINC_INSTALLROOT}/${BOINC_PROJECTNAME}/project.xml
done

echo "</boinc>" >> "$BOINC_INSTALLROOT"/"$BOINC_PROJECTNAME"/project.xml

echo "I (autodockvina_install_apps.sh): Invoking bin/xadd"
cd ${BOINC_INSTALLROOT}/${BOINC_PROJECTNAME}
./bin/xadd >> ${BOINC_INSTALLROOT}/${BOINC_PROJECTNAME}/install.log

yes | ./bin/update_versions >> ${BOINC_INSTALLROOT}/${BOINC_PROJECTNAME}/install.log

## Create templates
cp $BOINC_SOURCEDIR/autodockvina_templates/autodockvina_wu_template.xml ${BOINC_INSTALLROOT}/${BOINC_PROJECTNAME}/templates/
cp $BOINC_SOURCEDIR/autodockvina_templates/autodockvina_result_template.xml ${BOINC_INSTALLROOT}/${BOINC_PROJECTNAME}/templates/

## Create assimilator and validator
cd ${BOINC_INSTALLROOT}/${BOINC_PROJECTNAME}
if [ -f ./config.xml ]; then
  headconfigfile=$(mktemp /tmp/boinc_head_config_XXXXXX)
  tailconfigfile=$(mktemp /tmp/boinc_tail_config_XXXXXX)
  head -n -3 config.xml > $headconfigfile
  tail -n  3 config.xml > $tailconfigfile
  echo "I (autodockvina_install_apps.sh): Creating assimilator..."
  echo "<daemon>
      <cmd>
         sample_assimilator --app autodock-vina --sleep_interval 310 -d 3
      </cmd>
    </daemon>" >> $headconfigfile
  echo "I (autodockvina_install_apps.sh): Creating validator..."
  echo "<daemon>
      <cmd>
         sample_trivial_validator --app autodock-vina --sleep_interval 300 -d 3
      </cmd>
    </daemon>" >> $headconfigfile
  cat $tailconfigfile >> $headconfigfile
  mv $headconfigfile config.xml
  chmod 0644 config.xml
  rm $tailconfigfile
else
  echo "E (autodockvina_install_apps.sh): Configuration file 'config.xml' does not exist! Please check that the project directory exists."
  exit 1
fi

# restricting permissions
#echo "I (autodockvina_install_apps.sh): restricting permissions of directories and files in '$BOINC_INSTALLROOT/$BOINC_PROJECTNAME/apps'"
#find "$BOINC_INSTALLROOT"/"$BOINC_PROJECTNAME"/apps -type d | xargs -r sudo chmod go-w
#find "$BOINC_INSTALLROOT"/"$BOINC_PROJECTNAME"/apps -type f | xargs -r sudo chmod go-w
#echo "I (autodockvina_install_apps.sh): Not touching ownership of files in '$BOINC_INSTALLROOT/$BOINC_PROJECTNAME/app'"
#echo " (autodockvina_install_apps.sh)I (autodockvina_install_apps.sh): executing sudo chown -R $BOINC_USER '$BOINC_INSTALLROOT'/'$BOINC_PROJECTNAME'/app"
#sudo chown -R $BOINC_USER "$BOINC_INSTALLROOT"/"$BOINC_PROJECTNAME"/app

echo "I (autodockvina_install_apps.sh): cleaning up temp directories"
#rm -r ${TEMPDIR_VINA} ${TEMPDIR_APPS} ${TEMPDIR}
