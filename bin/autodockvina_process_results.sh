#!/bin/bash

set -e

version=0.0.0
if [ "-version" = "$1" -o "--version" = "$1" ]; then
   echo "$version"
   exit 1
fi

# Set configuration parameters
if [ -z "$BOINC_SOURCEDIR" ]; then BOINC_SOURCEDIR=$(dirname $0); fi

if [ "-h" = "$1" -o "-help" = "$1" -o "--help" = "$1" ]; then
        cat <<EOHELP
$(basename $0|tr "a-z" "A-Z")                   BOINC-SERVER-AUTODOCK                   $(basename $0|tr "a-z" "A-Z")

NAME

  $(basename $0) - process a set of docking results, writing
                   coordinates of ligand centers for further
                   analysis and visualisation, clustering them
                   and writing information about clusters.

SYNOPSIS

  $(basename $0) [--help]
  $(basename $0) <Directory with PDBQT files>

DESCRIPTION

  This script performs the following steps:

  1) Copies the first docked model from each PDBQT file found in <Directory with PDBQT files>
     to a file in <Directory with PDBQT files>_pdbqt. Input files should be named by template 
     specified by BOINC project settings.

  2) Writes to <Directory with PDBQT files>.dat the names of the ligands, coordinates of their 
     centers and the best docking energy values.

  3) Writes PyMol scripting commands to draw ligand centers to <Directory with PDBQT files>.pml.

  4) Clusters the ligand centers as points in 3-dimensional space and writes PyMol scripting 
     commands to draw cluster centers labeled with basic cluster information 
     to <Directory with PDBQT files>_clusters.pml.

SEE ALSO

  http://mgltools.scripps.edu
  http://autodock.scripps.edu

COPYRIGHT

  This script is released under the same license as BOINC.

AUTHORS

  The script was crafted by Natalia Nikitina (nevecie@yandex.ru).

EOHELP
        exit
fi

## Export the first one conformation for each of the docked ligands
if [ 1 -eq "$#" -a -d "$1" ]; then

  DIRNAME=$(basename $1)
  MODEL1_DIR=${DIRNAME}_pdbqt
  if [ -d $MODEL1_DIR ]; then
      mv $MODEL1_DIR ${MODEL1_DIR}_`date '+%s'`  
  fi
  mkdir $MODEL1_DIR

  for DOCKED_PDBQT in `find $1 -mindepth 1 -name "*_1"`
  do
    OUTFILE=`basename $DOCKED_PDBQT _1`
    cat $DOCKED_PDBQT | sed -n "/MODEL 1/,/MODEL 2/p" | head -n -1 > ${MODEL1_DIR}/${OUTFILE}_1
  done

  echo "  Results were exported to $MODEL1_DIR"

else

  echo "Please provide the name of the directory with output files in PDBQT format."
  exit 1

fi

## Get ligand name, center coordinates and the best docking energy value for each of the docked ligands
DATFILE=${DIRNAME}.dat

if [ -f $DATFILE ]; then
    mv $DATFILE ${DATFILE}_`date '+%s'`
fi

export DIRNAME=$DIRNAME
bash ${BOINC_SOURCEDIR}/autodockvina_generate_pymol_files.sh $MODEL1_DIR

echo "  Information about dockings was written to $DATFILE"

## Export the centers of docked ligands into *.pml file for further visualisation
PMLFILE=${DIRNAME}.pml
if [ -f $PMLFILE ]; then
    mv $PMLFILE ${PMLFILE}_`date '+%s'`
fi
awk '{print "pseudoatom ",$1,", color=teal, pos=[",$2,",",$3,",",$4,"]"}' $DATFILE > $PMLFILE
echo "  Ligand centers were written to $PMLFILE"

## Cluster the ligand centers and get information about each cluster
python -W ignore ./get_kmeans.py $DATFILE ${DIRNAME}_clusters.pml

