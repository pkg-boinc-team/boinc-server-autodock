#!/bin/bash

set -e

version=0.0.0
if [ "-version" = "$1" -o "--version" = "$1" ]; then
   echo "$version"
   exit 1
fi

if [ -z "$BOINC_SOURCEDIR" ]; then BOINC_SOURCEDIR=$(dirname $(realpath $0)); fi

## Set configuration parameters
if [ -r "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then
   . "$BOINC_SOURCEDIR"/autodockvina_set_config.sh
fi

if [ "-h" = "$1" -o "-help" = "$1" -o "--help" = "$1" ]; then
        cat <<EOHELP
$(basename $0|tr "a-z" "A-Z")                   BOINC-SERVER-AUTODOCK                   $(basename $0|tr "a-z" "A-Z")

NAME

  $(basename $0) - Get status of BOINC workunits for the autodock-vina application and given batch

SYNOPSIS

  $(basename $0) [--help]

DESCRIPTION

  This script retrieves the next batch number to create.
  All parameters are specified by environment variables.

     BOINC_PROJECTNAME  defines the unique short name of the project, which also serves
                        as the database name.
     BOINC_SOURCEDIR    defines the path to directory with intermediate scripts to install
                        and configure the BOINC project.
     BOINC_DBUSER       defines the user name for access to MySQL database.
     BOINC_DBPASS       defines the password for access to MySQL database.
     BATCH              defines the batch number (integer)

  The output is a single line containing the following numbers separated by space:
     TOTAL              total number of workunits in the batch        
     UNSENT             number of workunits yet unsent to BOINC clients
     INPROGRESS         number of workunits being processed by BOINC clients
     RECEIVED           number of workunits already received from BOINC clients
     ASSIMILATED        number of workunits already assimilated by the server

SEE ALSO

  http://wiki.debian.org/BOINC

COPYRIGHT

  This script is released under the same license as BOINC.

AUTHORS

  Natalia Nikitina

EOHELP
        exit
fi

if [ ! -r "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then
   echo "E: Could not read '$BOINC_SOURCEDIR/autodockvina_set_config.sh'"
   exit 1
fi

if [[ ! $1 =~ ^[0-9]+$ ]]; then
  echo "Batch number is not specified, getting status of all workunits..."
  WHEREBATCH="batch>=0"
else
  BATCH=$1
  WHEREBATCH="batch='$BATCH'"
fi

TOTAL=`mysql -u $BOINC_DBUSER -p$BOINC_DBPASS -N -s -e \
"use $BOINC_PROJECTNAME; select COUNT(id) from workunit where $WHEREBATCH;" 2> /dev/null`

# WU is considered unsent, if all its results are unsent
UNSENT=`mysql -u $BOINC_DBUSER -p$BOINC_DBPASS -N -s -e \
"use $BOINC_PROJECTNAME; select COUNT(distinct workunit.id) from workunit,result where \
result.workunitid=workunit.id and workunit.id not in (select workunitid from \
result where server_state<>'2') and workunit.$WHEREBATCH;" \
2> /dev/null`

# WU is considered received, if its assimilate_state is set to 1
RECEIVED=`mysql -u $BOINC_DBUSER -p$BOINC_DBPASS -N -s -e \
"use $BOINC_PROJECTNAME; select COUNT(id) from workunit where trim(assimilate_state)='1' and $WHEREBATCH;" \
2> /dev/null`

# WU is considered received, if its assimilate_state is set to 2
ASSIMILATED=`mysql -u $BOINC_DBUSER -p$BOINC_DBPASS -N -s -e \
"use $BOINC_PROJECTNAME; select COUNT(id) from workunit where trim(assimilate_state)='2' and $WHEREBATCH;" \
2> /dev/null`

let INPROGRESS=$TOTAL-$UNSENT-$RECEIVED-$ASSIMILATED && true

# The last output line for parsing from other programs
# (TODO: get rid of all other output)
echo "$TOTAL $UNSENT $INPROGRESS $RECEIVED $ASSIMILATED"
