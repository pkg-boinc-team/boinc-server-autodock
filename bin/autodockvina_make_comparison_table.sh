#!/bin/bash

set -e

version=0.0.0
if [ "-version" = "$1" -o "--version" = "$1" ]; then
   echo "$version"
   exit 1
fi

## Set configuration parameters
if [ -z "$BOINC_SOURCEDIR" ]; then BOINC_SOURCEDIR=$(dirname $0); fi

if [ "-h" = "$1" -o "-help" = "$1" -o "--help" = "$1" ]; then
        cat <<EOHELP
$(basename $0|tr "a-z" "A-Z")                   BOINC-SERVER-AUTODOCK                   $(basename $0|tr "a-z" "A-Z")

NAME

  $(basename $0) - process a set of docking results selecting the
                   specified number of the best ones and write a
                   table of Tanimoto coefficients between the ligands.

SYNOPSIS

  $(basename $0) [--help]
  $(basename $0) <Directory with PDBQT files of the docking results> <Number of ligands>

DESCRIPTION

  This script performs the following steps:

  1) Copies the first docked model from each PDBQT file found in <Directory with PDBQT files>
     to a file in <Directory with PDBQT files>_pdbqt. Input files should be named by template 
     specified by BOINC project settings.

  2) Writes to <Directory with PDBQT files>_energies.dat the names of the ligands and predicted
     docking energy values, sorted by increase of the latter.

  3) Selects <Number of ligands> first lines of the file and calculates Tanimoto coefficients
     between every two of these ligands. 
     The result is written to <Directory with PDBQT files>_table.dat.

  4) Plots to <Directory with PDBQT files>_table.png a diagram of Tanimoto coefficients as the
     measure of similarity between the ligands.  

SEE ALSO

  http://mgltools.scripps.edu
  http://autodock.scripps.edu

COPYRIGHT

  This script is released under the same license as BOINC.

AUTHORS

  The script was crafted by Natalia Nikitina (nevecie@yandex.ru).

EOHELP
        exit
fi


## Export the first one conformation for each of the docked ligands
if [ 3 -eq "$#" -a -d "$1" -a $2 -ge 0 2>/dev/null ]; then

  DIRNAME=$(basename $1)
  MODEL1_DIR=${DIRNAME}_pdbqt
  TABLEFILE=${DIRNAME}_table.dat
  DIAGRAMFILE=${DIRNAME}_table.png
  ENERGYFILE=${DIRNAME}_energies.dat
  TOP=$2
  SETNAME="$3"

  if [ -d $MODEL1_DIR ]; then
      mv $MODEL1_DIR ${MODEL1_DIR}_`date '+%s'`  
  fi
  mkdir $MODEL1_DIR

  for DOCKED_PDBQT in `find $1 -mindepth 1 -name "*_1"`
  do
    OUTFILE=`basename $DOCKED_PDBQT _1`
    # pdbqt file extension is needed for obabel to know file format
    cat $DOCKED_PDBQT | sed -n "/MODEL 1/,/MODEL 2/p" | head -n -1 > ${MODEL1_DIR}/${OUTFILE}_1.pdbqt
  done

  echo "  Results were exported to $MODEL1_DIR"

else

  echo "Please provide the name of the directory with output files in PDBQT format, 
        the number of the best results to be compared with each other and the dataset name."
  exit 1

fi

  if [ -f $TABLEFILE ]; then
     mv $TABLEFILE ${TABLEFILE}_`date '+%s'`
  fi

  ## Get the list of filenames + energies, sorted by energy:
  for PDBQT in ${MODEL1_DIR}/*_1.pdbqt
  do
      FILENAME=$(basename $PDBQT _1.pdbqt)
      E=$(grep -m 1 "VINA RESULT:" $PDBQT | sed 's/  */ /g' | cut -d' ' -f4)
      echo "$FILENAME $E"
  done | sed 's/  */ /g' | sort -n -k 2,2 | sed 's/ /,/g' > $ENERGYFILE

  ## Fill the table for the top $TOP ones:
  declare -A arr
  x=1
  for reflig in $(head -$TOP $ENERGYFILE)
  do
      REFLIGNAME=$(echo $reflig | cut -d',' -f1)
      ZINCID=$(expr $REFLIGNAME : '.*\(ZINC[0-9][0-9]*\)')
      ENERGY=$(echo $reflig | cut -d',' -f2)
      REF_LIG=${MODEL1_DIR}/${REFLIGNAME}_1.pdbqt
      echo -n "$ZINCID $ENERGY " >> $TABLEFILE
      y=1
      for testlig in $(head -$TOP $ENERGYFILE)
      do
          if [ -z ${arr[${x},${y}]} ]; then
            TESTLIGNAME=$(echo $testlig | cut -d',' -f1)
            TEST_LIG=${MODEL1_DIR}/${TESTLIGNAME}_1.pdbqt
            TANIMOTO_COEFF=$(obabel $REF_LIG $TEST_LIG ---errorlevel 1 -ofpt 2>/dev/null | awk -F' '\
                         "match(\$0,\"Tanimoto from $REF_LIG = ([0-9.]+)\",a){ print a[1]; exit 0 }")
            arr[${x},${y}]=$TANIMOTO_COEFF
            arr[${y},${x}]=$TANIMOTO_COEFF
          fi
          let y=y+1
      done
      for t in $(seq 1 $TOP); do echo -n ${arr[${x},${t}]}" " >> $TABLEFILE; done
      let x=x+1
      echo "" >> $TABLEFILE
  done

echo "The table of Tanimoto coefficients for $TOP best ligands was written to the file $TABLEFILE"
Rscript autodockvina_plot_results.R $TABLEFILE $SETNAME $DIAGRAMFILE   
echo "The diagram of Tanimoto coefficients for $TOP best ligands was plotted to the file $DIAGRAMFILE"                                                                                             
