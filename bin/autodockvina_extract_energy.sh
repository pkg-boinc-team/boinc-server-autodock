#!/bin/bash

set -e

version=0.0.0
if [ "-version" = "$1" -o "--version" = "$1" ]; then
   echo "$version"
   exit 1
fi

if [ "-h" = "$1" -o "-help" = "$1" -o "--help" = "$1" -o -z "$1" ]; then
	cat <<EOHELP
$(basename $0|tr "a-z" "A-Z")			BOINC-SERVER-AUTODOCK			$(basename $0|tr "a-z" "A-Z")

NAME

  $(basename $0) - retrieve predicted binding energies

SYNOPSIS

  $(basename $0) [--help] 
  $(basename $0) <result files>
  $(basename $0) <single directory with result files>

DESCRIPTION

  This script processes output files created by a BOINC project 
  for AutoDock Vina Drug Screening. The filenames are given as
  command line arguments.

SEE ALSO

  http://mgltools.scripps.edu
  http://autodock.scripps.edu

COPYRIGHT

  This script is released under the same license as BOINC.

AUTHORS

  The script was crafted by Natalia Nikitina (nevecie@yandex.ru)
  and extended by Steffen Moeller (moeller@debian.org).


EOHELP
	exit
fi

if [ 1 -eq "$#" -a -d "$1" ]; then

	find "$1" -name "*_1" | xargs -r $0

else

	for n in $*
	do
		echo -n $(basename $n)
                E=$(grep -m 1 "VINA RESULT:" $n | sed 's/  */ /g' | cut -d' ' -f4)
                echo " $E"
	done

fi
