#!/bin/bash

set -e

version=
if [ "-version" = "$1" -o "--version" = "$1" ]; then
   echo "$version"
   exit 1
fi

verbose="yes"

if [ -z "$BOINC_SOURCEDIR" ]; then BOINC_SOURCEDIR=$(dirname $(realpath $0)); fi

if [ -r "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then
   . "$BOINC_SOURCEDIR"/autodockvina_set_config.sh
fi

if [ "-h" = "$1" -o "-help" = "$1" -o "--help" = "$1" ]; then
        cat <<EOHELP
$(basename $0|tr "a-z" "A-Z")                   BOINC-SERVER-AUTODOCK                   $(basename $0|tr "a-z" "A-Z")

NAME

  $(basename $0) - Generation of BOINC workunits for the autodockvina application

SYNOPSIS

  $(basename $0) [--help] <single_receptor.pdbqt> <config_file_for_docking> <directory_with_ligands>

DESCRIPTION

  This script generates BOINC workunits for the autodockvina application,
  using the specified receptor file and the specified set of ligand files.

  The '--cpu 1' command line option is set for AutoDock Vina to limit the 
  resources usage for a single BOINC workunit.  

  The following parameters are specified by environment variables:

    BOINC_PROJECTNAME      defines the unique short name of the project, which also serves
                           as the database name ($BOINC_PROJECTNAME).
    BOINC_SOURCEDIR        defines the path to directory with intermediate scripts to install
                           and configure the BOINC project ($BOINC_SOURCEDIR).
    BOINC_INSTALLROOT      defines the path to the directory where to install the BOINC project ($BOINC_INSTALLROOT).
    BOINC_DBUSER           defines the user name that has access to the project's MySQL database ($BOINC_DBUSER).
    BOINC_DBPASS           defines the password for access to MySQL database ($BOINC_DBPASS).

  The scripts needs 'realpath' to function.

SEE ALSO

  http://wiki.debian.org/BOINC
  http://mgltools.scripps.edu
  http://autodock.scripps.edu

COPYRIGHT

  This script is released under the same license as BOINC.

AUTHORS

  Natalia Nikitina with contributions by Steffen Moeller

EOHELP
        exit
fi

## Set configuration parameters
if [ ! -r "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then
   echo "E: Cannot find \$BOINC_SOURCEDIR/autodockvina_set_config.sh"
   exit 1
fi

BATCH=`mysql -u $BOINC_DBUSER -p$BOINC_DBPASS -N -s -e "use $BOINC_PROJECTNAME; select MAX(batch) from workunit;" 2> /dev/null` 
if [ -z "$BATCH" ]; then
  echo "E: Error selecting batch number from the database! Please check MySQL connection parameters."
  exit 1
else
  let BATCH+=1  
fi

RECEPTORFILE=$1

if [ -d "$RECEPTORFILE" ]; then
   echo "E: Only a single pdbqt file featuring a receptor is accepted."
   exit 1
fi

RECEPTORFILE=$(realpath $RECEPTORFILE)

if ! echo "$RECEPTORFILE" | egrep -q '\.pdbqt$' ; then
   echo "E: Please have .pdbqt as part of the receptor name."
   exit 1
fi

CONFIGFILE=$2

if [ -d "$CONFIGFILE" ]; then
   echo "E: Only a single file with all the docking instructions for Vina is accepted."
   exit 1
fi

CONFIGFILE=$(realpath $CONFIGFILE)


LIGANDSDIR=$3

if [ ! -d "$LIGANDSDIR" ]; then
   echo "E: Expected a directory with .pdbqt-formatted ligands at '$LIGANDSDIR'"
   exit 1
fi

LIGANDSDIR=$(realpath $LIGANDSDIR)


RECEPTOR=$(basename $RECEPTORFILE .pdbqt)
CONFIG=$(basename $CONFIGFILE)

cd ${BOINC_INSTALLROOT}/${BOINC_PROJECTNAME}        #Change to the directory where BOINC project is installed to
ln -sf $RECEPTORFILE .
./bin/stage_file --copy $RECEPTOR.pdbqt
ln -sf $CONFIGFILE $CONFIG
./bin/stage_file --copy $CONFIG   

for lig_file in $(find $LIGANDSDIR -name '*.pdbqt') #Cycle over the set of ligand files
do

   if [ -n "$verbose" ]; then
      echo "I: creating work for '$lig_file'"
   fi

   lig_name=`basename $lig_file .pdbqt`
   ligand_input=ligand_input_tmp_${lig_name}_${i}`date '+%s'`
   ln -s $lig_file $ligand_input

   #Stage input files:
   ./bin/stage_file --copy $ligand_input

   #Generate workunit:
   wuname=${RECEPTOR}_${lig_name}_${BATCH}_`date '+%s'`
   bin/create_work --appname autodock-vina --batch $BATCH --wu_name $wuname --wu_template templates/autodockvina_wu_template.xml \
                   --result_template templates/autodockvina_result_template.xml \
                   --command_line "--cpu 1 --receptor receptor.pdbqt --ligand ligand.pdbqt --config config.txt --out vina_result.pdbqt" $ligand_input $RECEPTOR.pdbqt $CONFIG

   rm -f $ligand_input

done

rm -f $RECEPTOR.pdbqt $CONFIG

echo "I: Workunits were successfully created for batch #$BATCH"

