#!/bin/bash

set -e

version=0.0.0
if [ "-version" = "$1" -o "--version" = "$1" ]; then
   echo "$version"
   exit 1
fi

if [ -z "$BOINC_SOURCEDIR" ]; then BOINC_SOURCEDIR=$(dirname $(realpath $0)); fi

## Set configuration parameters
if [ -r "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then
   . "$BOINC_SOURCEDIR"/autodockvina_set_config.sh
fi

if [ "-h" = "$1" -o "-help" = "$1" -o "--help" = "$1" ]; then
        cat <<EOHELP
$(basename $0|tr "a-z" "A-Z")                   BOINC-SERVER-AUTODOCK                   $(basename $0|tr "a-z" "A-Z")

NAME

  $(basename $0) - installation of a BOINC project

SYNOPSIS

  $(basename $0) [--help]

DESCRIPTION

  This script makes the BOINC project with specified name, basing on the prepared database,
  and prepares the web pages to access or administrate the project. 

  All parameters are specified by environment variables.

    BOINC_PROJECTNAME      defines the unique short name of the project, which also serves
                           as the database name.
    BOINC_PROJECTNICENAME  defines the full name of the project, which will be displayed on
                           the project webpages and in the BOINC clients software.
    BOINC_SOURCEDIR        defines the path to directory with intermediate scripts to install
                           and configure the BOINC project.
    BOINC_INSTALLROOT      defines the path to the directory where to install the BOINC project.
    BOINC_USER             defines the user name under which the BOINC project will be run.
    BOINC_WWWGROUP         defines the users group that will have HTTP access to download/upload
                           files on the server. 
    BOINC_DBUSER           defines the user name that has access to the project's MySQL database.
    BOINC_DBPASS           defines the password for access to MySQL database.
    PYTHONPATH             defines the path to search for the Python executable

SEE ALSO

  http://wiki.debian.org/BOINC
  http://mgltools.scripps.edu
  http://autodock.scripps.edu

COPYRIGHT

  This script is released under the same license as BOINC.

AUTHORS

  Natalia Nikitina

EOHELP
        exit
fi

if [ ! -r "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then
   echo "E: Could not read '$BOINC_SOURCEDIR/autodockvina_set_config.sh'"
   exit 1
fi

## Create the BOINC project
echo "I: Creating the project..."
hostip=`hostname -I | head -n 1`              #IP address of the server
hosturl=`echo "http://${hostip}" | tr -d ' '` #trim whitespaces
echo -n "I: Basic configuration test: "
if [ -z "$BOINC_INSTALLROOT" -o -z "$hosturl" -o -z "$BOINC_PROJECTNAME" -o -z "$BOINC_DBPASS" \
  -o -z "$BOINC_PROJECTNICENAME" -o -z "$BOINC_PROJECTNAME" ] ; then
  echo "E: Missing a configuration parameter. Please check the configuration."
  exit 1
else
  echo "[ok]"
fi

## Call make_project utility to register the BOINC project
if ! PYTHONPATH=$PYTHONPATH:/usr/share/pyshared/Boinc/ \
 /usr/share/boinc-server-maker/tools/make_project \
  --url_base "$BOINC_PROJECTURL" \
  --db_name "$BOINC_PROJECTNAME" \
  --db_user "$BOINC_DBUSER" \
  --db_passwd "$BOINC_DBPASS" \
  --delete_prev_inst --drop_db_first  \
  --project_root "$BOINC_INSTALLROOT"/"$BOINC_PROJECTNAME" \
  --srcdir /usr/share/boinc-server-maker/ \
   "$BOINC_PROJECTNAME" "$BOINC_PROJECTNICENAME"
then
  echo "E: Error making project! Please make sure that the configuration parameters 
        are correct and try again." 
  exit 1
fi

## Add automated restart upon failure
crontabfile=$(mktemp /tmp/boinc_crontab_XXXXXX)
echo "I: Adding automated restart upon failure... executing sudo crontab -u $BOINC_USER"
if ! sudo crontab -u $BOINC_USER -l | grep -v "$BOINC_INSTALLROOT/$BOINC_PROJECTNAME/bin/start" > $crontabfile; then
  echo "W: ignore crontab error if this indicates not to have previous entries"
fi
echo "*/5 * * * * $BOINC_INSTALLROOT/$BOINC_PROJECTNAME/bin/start --cron" >> $crontabfile
echo "I: executing sudo crontab -u $BOINC_USER"
sudo crontab -u $BOINC_USER "$crontabfile"
rm "$crontabfile"

## Set access to administrative web interface (will ask to enter the new password)
echo "I: Setting access to administrative web interface..."
cd "${BOINC_INSTALLROOT}"/"${BOINC_PROJECTNAME}"
htpasswd -c html/ops/.htpasswd $BOINC_USER

## Configure Apache to call BOINC server
if ! grep -q Require ${BOINC_PROJECTNAME}.httpd.conf; then
  # Apache 2.4 needs this but the regular BOINC code is still at 2.2
  sed -i '/Order/i\ \ \ \ \ \ \ \ Require all granted' ${BOINC_PROJECTNAME}.httpd.conf
  echo "I: Added 'Require' statement for Apache 2.4 compliance of ${BOINC_PROJECTNAME}.httpd.conf"
fi

echo "I: executing sudo ${BOINC_PROJECTNAME}.httpd.conf /etc/apache2/sites-available/"
sudo cp "${BOINC_PROJECTNAME}.httpd.conf"  /etc/apache2/sites-available/
echo "I: executing sudo a2ensite ${BOINC_PROJECTNAME}.httpd.conf"
sudo a2ensite  "${BOINC_PROJECTNAME}.httpd.conf"
echo "I: executing sudo a2enmod cgi"
sudo a2enmod cgi
echo "I: executing sudo a2enmod php5"
sudo a2enmod php5
echo "I: executing sudo apache reload"
sudo /etc/init.d/apache2 reload

## Make html pages look nice and create the forums
echo "I: Adjusting HTML pages for project in '$BOINC_INSTALLROOT/$BOINC_PROJECTNAME"
cd "${BOINC_INSTALLROOT}"/"${BOINC_PROJECTNAME}"
sed -i "s/REPLACE WITH PROJECT NAME/$BOINC_PROJECTNICENAME/" html/project/project.inc
sed -i "s/REPLACE WITH COPYRIGHT HOLDER/The Debian Community/" html/project/project.inc
if ! cp "${BOINC_SOURCEDIR_ABS}/html/user/index.php" "html/user/index.php"; then
  echo "E: Error copying ${BOINC_SOURCEDIR_ABS}/html/user/index.php to html/user/index.php"
  echo "E: Please report."
  echo "E:    BOINC_SOURCEDIR:     $BOINC_SOURCEDIR"
  echo "E:    BOINC_SOURCEDIR_ABS: $BOINC_SOURCEDIR_ABS"
  echo "E:    BOINC_INSTALLROOT:   $BOINC_INSTALLROOT"
  echo "E:    BOINC_PROJECTNAME:   $BOINC_PROJECTNAME"
  echo "E:    PWD:                 $(pwd)"
  exit 1
fi
sed -i 's/PROJECT/"AutoDockVina@Home"/g' html/user/index.php
# The following may be something for upstream to consider
sed -i 's%/etc/httpd/run/httpd.pid%/var/run/apache2/apache2.pid%' html/user/server_status.php
#sed -i 's/URL_BASE/therightURL/g' html/user/index.php              # to be added
#sed -i 's/STYLESHEET/additionalStylesheet/g' html/user/index.php   # to be added

cd "${BOINC_INSTALLROOT}"/"${BOINC_PROJECTNAME}"/html/ops
sed -i '/remove the die/d' create_forums.php
php5 create_forums.php

## Set permissions for upload directory, logs directory and html pages hierarchy

echo "I: Setting permissions..."
cd "${BOINC_INSTALLROOT}"/"${BOINC_PROJECTNAME}"
echo "I: not changing ownerships of ${BOINC_INSTALLROOT}/${BOINC_PROJECTNAME} at this point"
#echo "I: executing sudo chown -R $BOINC_USER:$BOINC_WWWGROUP in $(pwd)"
#sudo chown -R $BOINC_USER:$BOINC_WWWGROUP .
echo "I: executing sudo chmod g+w -R $(pwd)"
sudo chmod g+w -R .
echo "I: executing sudo chmod 02770 -R upload etc."
sudo chmod 02770 -R upload html/cache html/inc html/languages html/languages/compiled html/user_profile
echo -n "I: executing sudo chgrp www-data, log, upload: "
sudo chgrp -R www-data log_"$BOINC_HOSTNAME" upload && echo "[ok]" || echo "[failed]"
if [ -d html/inc -a -d cgi-bin ]; then
  echo -n "html/inc: "
  chmod o+x html/inc && chmod -R o+r html/inc && echo "[ok]" || echo "[failed]"
  echo -n "html/languages: "
  chmod o+x html/languages/ html/languages/compiled && echo "[ok]" || echo "[failed]"
else
  echo "E: Error setting permissions: you are not in your project directory."
  exit 1
fi

echo "I:
      The Apache server has been configured to call BOINC server.
      Please check that the project website is displayed correctly.
      Make any necessary changes to the start page by editing 
      $BOINC_INSTALLROOT/$BOINC_PROJECTNAME/html/project/project.inc
      and $BOINC_INSTALLROOT/$BOINC_PROJECTNAME/html/user/index.php"
