#!/bin/bash

set -e

version=
if [ "-version" = "$1" -o "--version" = "$1" ]; then
   echo "$version"
   exit 1
fi

## Set configuration parameters
if [ -z "$BOINC_SOURCEDIR" ]; then BOINC_SOURCEDIR=$(dirname $(realpath $0)); fi

## Set configuration parameters
if [ -r "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then
   . "$BOINC_SOURCEDIR"/autodockvina_set_config.sh
fi

if [ "-h" = "$1" -o "-help" = "$1" -o "--help" = "$1" ]; then

   cat <<EOHELP
$(basename $0|tr "a-z" "A-Z")			BOINC-SERVER-AUTODOCK			$(basename $0|tr "a-z" "A-Z")

NAME

  $(basename $0) - installation of complete BOINC Server for AutoDock Vina Drug Screening

SYNOPSIS

  $(basename $0) [--help] 

DESCRIPTION

  This script prepares the database, installs, configures and launches the BOINC server 
  for virtual drug screening using AutoDock Vina. For commands in demand of root privileges
  it will invoke sudo.

  All parameters are specified by environment variables.

     BOINC_PROJECTNAME   defines the unique short name of the project,
                         which also serves as the database name. It is currently set
			 to '$BOINC_PROJECTNAME'.
     BOINC_SOURCEDIR     defines the path to a directory with intermediate
                         scripts to install and configure the BOINC project. It is
                         currently set to '$BOINC_SOURCEDIR'.
     BOINC_USER          defines the user name under which the BOINC project will be run,
                         currently set to '$BOINC_USER'.
     BOINC_WWWGROUP      defines the users group that will have HTTP access to download/upload
                         files on the server. If \$BOINC_USER currently does not belong to the 
                         \$BOINC_WWWGROUP, the script adds them.
                         currently set to '$BOINC_WWWGROUP'.
     BOINC_DBUSER        defines the user name that will have access to MySQL database.
                         currently set to '$BOINC_DBUSER'.
     BOINC_DBPASS        defines the password for access to MySQL database.
                         currently set to '$BOINC_DBPASS'.
     BOINC_INSTALLROOT   directory at which to install the complete project,
                         which will also have the public web pages. Currently
                         set to '$BOINC_INSTALLROOT.

  Please be aware that the script deletes the previous installation of any BOINC project with
  the same BOINC_PROJECTNAME.

SEE ALSO

  http://wiki.debian.org/BOINC
  http://mgltools.scripps.edu
  http://autodock.scripps.edu
EOHELP

if [ -n "$BOINC_SOURCEDIR" ]; then
  cat <<EOHELP
  file://$(dirname $BOINC_SOURCEDIR)/autodockvina_install_project.sh
  file://$(dirname $BOINC_SOURCEDIR)/autodockvina_install_apps.sh
EOHELP
fi

cat <<EOHELP

COPYRIGHT

  This script is released under the same license as BOINC.

AUTHORS

  The script was crafted by Natasha Nikitina, based on the Wiki pages
  of the Debian Med BOINC project.

EOHELP
	exit 1
fi


if [ ! -r "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then
   echo "E: Could not read '$BOINC_SOURCEDIR/autodockvina_set_config.sh'"
   exit 1
fi

if [ -z "$BOINC_INSTALLROOT" ]; then
   echo "E: specify 'BOINC_INSTALLROOT' variable."
   exit 1
fi

if [ -z "$BOINC_PROJECTNAME" ]; then
   echo "E: specify 'BOINC_PROJECTNAME' variable."
   eixt 1
fi

if ! (( ypcat passwd | grep -q "$BOINC_USER" ) || grep -q $BOINC_USER /etc/passwd )  ; then
   sudo useradd $BOINC_USER -d "$BOINC_INSTALLROOT" -s "/bin/bash"
fi

if ! groups "$BOINC_USER" | grep -q "$BOINC_WWWGROUP"; then 
   echo "I: User '$BOINC_USER' is not in '$BOINC_WWWGROUP' group, adding..."
   echo "I: Executing with sudo: usermod -aG '$BOINC_WWWGROUP' '$BOINC_USER'"
   sudo usermod -aG "$BOINC_WWWGROUP" "$BOINC_USER"
fi

if ! [ -d "$BOINC_INSTALLROOT" ]; then
   echo "I: Creating '$BOINC_INSTALLROOT' directory"
   sudo mkdir "$BOINC_INSTALLROOT"
   sudo chmod 777 $BOINC_INSTALLROOT
fi


## Prepare MySQL database, deleting the previous one with the same name if it existed
echo "I: Preparing MySQL database... enter MySQL password for root upon request"

cat <<EOMYSQL | mysql -u root -p;
DROP DATABASE IF EXISTS ${BOINC_PROJECTNAME};
GRANT ALL PRIVILEGES ON ${BOINC_PROJECTNAME}.* TO '${BOINC_DBUSER}'@'localhost' IDENTIFIED BY '${BOINC_DBPASS}';
EOMYSQL

## removing project of same name at same installroot
if [ -d "$BOINC_INSTALLROOT"/"$BOINC_PROJECTNAME" ]; then
   echo "I: Removing '$BOINC_INSTALLROOT'/'$BOINC_PROJECTNAME'"
   sudo rm -r "$BOINC_INSTALLROOT"/"$BOINC_PROJECTNAME"
fi

## Create the BOINC project
. ${BOINC_SOURCEDIR}/autodockvina_install_project.sh

## Install AutoDock Vina application
. ${BOINC_SOURCEDIR}/autodockvina_install_apps.sh

echo "I: Changing ownership of '$BOINC_INSTALLROOT' directory to '$BOINC_USER'"
sudo chown -R $BOINC_USER $BOINC_INSTALLROOT
sudo chmod 775 $BOINC_INSTALLROOT
sudo chgrp -R www-data $BOINC_INSTALLROOT/$BOINC_PROJECTNAME/html

## Start the BOINC project
echo "I: The project has been successfully created. Starting..."
sudo -u ${BOINC_USER} ./bin/start
