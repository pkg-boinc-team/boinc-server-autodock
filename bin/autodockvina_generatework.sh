#!/bin/bash

# (the script is under development)

set -e

version=0.0.0
if [ "-version" = "$1" -o "--version" = "$1" ]; then
   echo "$version"
   exit 1
fi

if [ -z "$BOINC_SOURCEDIR" ]; then BOINC_SOURCEDIR=$(dirname $(realpath $0)); fi

## Set configuration parameters
if [ -r "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then
   . "$BOINC_SOURCEDIR"/autodockvina_set_config.sh
fi

if [ "-h" = "$1" -o "-help" = "$1" -o "--help" = "$1" ]; then
        cat <<EOHELP
$(basename $0|tr "a-z" "A-Z")                   BOINC-SERVER-AUTODOCK                   $(basename $0|tr "a-z" "A-Z")

NAME

  $(basename $0) - generation of BOINC workunits for the AutoDock Vina application

SYNOPSIS

  $(basename $0) [--help]

DESCRIPTION

  This script generates a single BOINC workunit with a new unique batch number.
  For high throughput screening this is not what you want.


SEE ALSO

  TODO: mention what should be run for HTS

  http://wiki.debian.org/BOINC
  http://mgltools.scripps.edu
  http://autodock.scripps.edu

COPYRIGHT

  This script is released under the same license as BOINC.

AUTHORS

  Natalia Nikitina

EOHELP
        exit
fi


if [ ! -r "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then
   echo "E: Could not read '$BOINC_SOURCEDIR/autodockvina_set_config.sh'"
   exit 1
fi

#TODO: replace with the call to get_batch.sh
BATCH=`mysql -u $BOINC_DBUSER -p$BOINC_DBPASS -N -s -e "use $BOINC_PROJECTNAME; select MAX(batch) from workunit;" 2> /dev/null` 
if [ -z "$BATCH" ]; then
  echo "E: Error selecting batch number from the database! Please check MySQL connection parameters."
  exit 1
else
  let BATCH+=1  
fi

cd ${BOINC_INSTALLROOT}/${BOINC_PROJECTNAME}
ligand_input=ligand_input_tmp_${i}`date '+%s'`
receptor_input=receptor_input_tmp_${i}`date '+%s'`
config=config_tmp_${i}`date '+%s'`

#TODO: make input files the parameters
cp ${BOINC_HOMEDIR}/my_autodock_vina_library/ligand.pdbqt $ligand_input
cp ${BOINC_HOMEDIR}/my_autodock_vina_library/receptor.pdbqt $receptor_input
cp ${BOINC_HOMEDIR}/my_autodock_vina_library/config.txt $config

mv $ligand_input `sg www-data -c "./bin/dir_hier_path $ligand_input"`
echo "I: Created input file $ligand_input..."
mv $receptor_input `sg www-data -c "./bin/dir_hier_path $receptor_input"`
echo "I: Created input file $receptor_input..."
mv $config `sg www-data -c "./bin/dir_hier_path $config"`
echo "I: Created input file $config..."

bin/create_work --appname autodock-vina --batch $BATCH --wu_template templates/autodockvina_wu_template.xml \
                --result_template templates/autodockvina_result_template.xml \
                --command_line "--receptor receptor.pdbqt --ligand ligand.pdbqt --config config.txt --out vina_result.pdbqt" $ligand_input $receptor_input $config

echo "I: Workunits were successfully created for batch #$BATCH"

