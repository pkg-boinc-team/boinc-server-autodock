set -e

if [ -z "$BOINC_SOURCEDIR" ]; then BOINC_SOURCEDIR=$(dirname $(realpath $0)); fi

## Set configuration parameters
if [ -r "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then
   . "$BOINC_SOURCEDIR"/autodockvina_set_config.sh
fi

if [ "-h" = "$1" -o "" = "$1" -o "--help" = "$1" ]; then
        cat <<EOHELP
$(basename $0|tr "a-z" "A-Z")                   BOINC-SERVER-AUTODOCK                   $(basename $0|tr "a-z" "A-Z")

NAME

$(basename $0) - retrieve top predicted binding energies with ligand names

SYNOPSIS

$(basename $0) [--help] [OUTPUTSDIR] [TOP]

DESCRIPTION

  This script processes output files created by a BOINC project
  for AutoDock Vina Drug Screening. 

  All parameters are specified by command line arguments.

     OUTPUTSDIR   defines directory with output files ($OUTPUTSDIR)
     TOP          defines the number of ligands with the smallest 
                  values of predicted binding energies to display
		  ($TOP)

SEE ALSO

  http://mgltools.scripps.edu
  http://autodock.scripps.edu

COPYRIGHT

  This script is released under the same license as BOINC.

AUTHORS

  The script was crafted by Natalia Nikitina (nevecie@yandex.ru)

EOHELP
        exit
fi


if [ ! -r "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then
   echo "E: Could not read '$BOINC_SOURCEDIR/autodockvina_set_config.sh'"
   exit 1
fi

## Get the number of ligands to display and the working directory
#TOP=10
OUTPUTSDIR='.'
if [ $# -ge 1 ] ; then
  if [ -d $1 ] ;  then
    OUTPUTSDIR=$1
    if [ $# -ge 2 ] ; then
      if ! [[ $2 =~ ^[0-9]+$ ]] ; then
        echo "Please, specify the number of hits properly."
        exit
      else
        TOP=$2
        ./autodockvina_extract_energy.sh ${OUTPUTSDIR} | sort -n -k 2,2 | head -$TOP
      fi
    else
      ./autodockvina_extract_energy.sh ${OUTPUTSDIR} | sort -n -k 2,2
    fi
  else
    echo "Please, specify number of hits and the directory with result files."
    exit
  fi
fi
