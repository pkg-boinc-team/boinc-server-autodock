#!/bin/bash

set -e

version=0.0.0
if [ "-version" = "$1" -o "--version" = "$1" ]; then
   echo "$version"
   exit 1
fi

if [ -z "$BOINC_SOURCEDIR" ]; then BOINC_SOURCEDIR=$(dirname $(realpath $0)); fi

## Set configuration parameters
if [ -r "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then
   . "$BOINC_SOURCEDIR"/autodockvina_set_config.sh
fi

if [ "-h" = "$1" -o "-help" = "$1" -o "--help" = "$1" ]; then
        cat <<EOHELP
$(basename $0|tr "a-z" "A-Z")                   BOINC-SERVER-AUTODOCK                   $(basename $0|tr "a-z" "A-Z")

NAME

  $(basename $0) - cancel the workunits of the given batch

SYNOPSIS

  $(basename $0) [--help]

DESCRIPTION

  This script marks the workunits of the specified batch as cancelled.
  All parameters are specified by environment variables.

     BOINC_PROJECTNAME   defines the unique short name of the project, which also serves
                         as the database name.
     BOINC_SOURCEDIR     defines the path to directory with intermediate scripts to install
                         and configure the BOINC project.
     BOINC_DBUSER        defines the user name for access to MySQL database.
     BOINC_DBPASS        defines the password for access to MySQL database.
     BATCH               defines the batch number (integer)

SEE ALSO

  http://wiki.debian.org/BOINC

COPYRIGHT

  This script is released under the same license as BOINC.

AUTHORS

  Natalia Nikitina

EOHELP
        exit
fi

if [ ! -r "$BOINC_SOURCEDIR"/autodockvina_set_config.sh ]; then
   echo "E: Could not read '$BOINC_SOURCEDIR/autodockvina_set_config.sh'"
   exit 1
fi


if [[ ! $1 =~ ^[0-9]+$ ]]; then
  echo "Batch number is not specified!" 
  exit 1
else
  BATCH=$1
  WHEREBATCH="batch='$BATCH'"
fi

## Get the IDs of workunits to cancel
WU_IDS=`mysql -u $BOINC_DBUSER -p$BOINC_DBPASS -N -s -e \
"use $BOINC_PROJECTNAME; select MIN(id),MAX(id) from workunit where batch='$BATCH';" \
2> /dev/null | tr -s ' '`

WU_ID1=$(echo $WU_IDS | cut -f1 -d' ')
WU_ID2=$(echo $WU_IDS | cut -f2 -d' ')

mysql -u $BOINC_DBUSER -p$BOINC_DBPASS -N -s -e "use $BOINC_PROJECTNAME; update result set server_state=5, outcome=5 where server_state=2 and $WU_ID1<=workunitid and workunitid<=$WU_ID2 and $WHEREBATCH;"

mysql -u $BOINC_DBUSER -p$BOINC_DBPASS -N -s -e "use $BOINC_PROJECTNAME; update workunit set error_mask=error_mask|16 where $WU_ID1<=id and id<=$WU_ID2 and $WHEREBATCH;"

exit 0
