#!/bin/bash

set -e

version=0.0.0
if [ "-version" = "$1" -o "--version" = "$1" ]; then
   echo "$version"
   exit 1
fi

if [ "-h" = "$1" -o "-help" = "$1" -o "--help" = "$1" ]; then
        cat <<EOHELP
$(basename $0|tr "a-z" "A-Z")                   BOINC-SERVER-AUTODOCK                   $(basename $0|tr "a-z" "A-Z")

NAME

  $(basename $0) - show RMS calculated between each of the
                   sample ligands and the reference ligand,
                   docked against the same protein.

SYNOPSIS

  $(basename $0) [--help]
  $(basename $0) <reference ligand in PDB format>

DESCRIPTION

  This script outputs the RMS between each ligand found in the same
  directory as the given reference ligand. Ligands should be described
  in PDB format. A script to convert a collection of PDBQT results to 
  PDB files will follow. 

SEE ALSO

  http://mgltools.scripps.edu
  http://autodock.scripps.edu
  http://www.pymol.org

COPYRIGHT

  This script is released under the same license as BOINC.

AUTHORS

  The script was crafted by Natalia Nikitina (nevecie@yandex.ru).


EOHELP
        exit
fi

if [ 1 -eq "$#" -a -f "$1" ]; then

  LIG_DIR=`dirname "$1"`
  LIG_REF=`basename "$1"`
  echo "=== Reference ligand '$LIG_REF' in directory '$LIG_DIR' ==="
  echo -e "Sample ligand filename\tRMS"
  for LIG_TEST in `find ${LIG_DIR} -name "*.pdb" -printf "%f\n"`
  do
    echo -n -e "${LIG_TEST}\t"
    pymol -c -d "load ${LIG_DIR}/${LIG_REF}, ref; load ${LIG_DIR}/${LIG_TEST}, test; rms_cur ref, test;" \
    | grep 'Executive' | sed 's/  */ /g' | cut -d' ' -f5
  done

fi

